#!/usr/bin/env python3

from termcolor import colored, cprint

class ConsoleLog():
   verbose = 0
   colors = { "info": "blue","error": "red","done": "green","undone": "yellow","waiting": "magenta" }
   symbol = { "info": "i","error": "!","done": "+","undone": "-", "waiting": "*" }
   verboseLevel = { "info": 2, "error": 0, "done": 0, "undone": 1, "waiting": 0 }

   def __init__(self, verbose):
      self.verbose = verbose

   def output(self, output, level, endString='\n' ):

      if self.verbose >= self.verboseLevel[level]:
         cprint("["+self.symbol[level]+"]", self.colors[level], attrs=['bold'], end='' )
         cprint(output, end=endString)
