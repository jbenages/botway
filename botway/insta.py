#!/usr/bin/python3
import instaloader,os
import pandas as pd
from datetime import datetime,timedelta

class Insta(object):
    def __init__(self, user, password, userAgent):
        self.user = user
        self.password = password
        self.userAgent = userAgent
        self.connection = instaloader.Instaloader(user_agent=userAgent)
        try:
            self.connection.load_session_from_file(user, user)
        except:
            self.connection.login(user=user,passwd=password)
            self.connection.save_session_to_file(user + password)
        self.url = "https://www.instagram.com/p/"

    def __enter__(self):
        return self.connection

    def __exit__(self, ctx_type, ctx_value, ctx_traceback):
        del self.connection

    def downloadPost(self, profileName, postShortCode):
        profile = instaloader.Profile.from_username(self.connection.context, profileName)
        post = instaloader.Post.from_shortcode(self.connection.context,postShortCode)
        self.connection.download_post(post, "%s-%s" % (profileName, postShortCode))
    
    def getPostsShortCode(self, profile, hoursPassed=24, maxIteration=3):
        profile = instaloader.Profile.from_username(self.connection.context, profile)
        posts = profile.get_posts()
        SINCE = datetime.now() - timedelta(hours = hoursPassed)  # further from today, inclusive
        
        postsShortCodes = []

        for index, post in enumerate(posts, 1):
            postdate = post.date
            if index > maxIteration:
                break
            else:
                if postdate >= SINCE:
                    postsShortCodes.append(vars(post)["_node"]["shortcode"])
        
        return postsShortCodes

