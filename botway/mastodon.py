#!/usr/bin/python3
import requests

class Mastodon(object):
    def __init__(self, urlInstance,token, headerText= "Author: {author}. Publish Date: {date}",footerText= "Source: {link}",maxLengthPost=495):
        self.url = urlInstance
        self.token = token
        self.maxLengthPost = maxLengthPost
        self.headerText = headerText
        self.footerText = footerText

    def __enter__(self):
        return self.url

    def __exit__(self, ctx_type, ctx_value, ctx_traceback):
        del self.url

    def convertTextPost(self,author,date,link,text):
        joinedText = self.headerText.format(author=author,date=date) + "\n\n" + text + "\n" + self.footerText.format(link=link)
        convertedText=[joinedText[y-self.maxLengthPost:y] for y in range(self.maxLengthPost, len(joinedText)+self.maxLengthPost,self.maxLengthPost)]
        return convertedText

    def uploadMedia(self,filePath,description="Image description"):
        r = requests.post(
            self.url + '/api/v2/media',
            files={'file': open(filePath, 'rb')},
            data={
                'description': description},
            headers={'Authorization': 'Bearer %s' % (self.token) },
        )
        return r.json()["id"]

    def replyPost(self,text,idParent):
        json = {'status': text, 'in_reply_to_id': idParent}

        r = requests.post(
            self.url + '/api/v1/statuses',
            json=json,
            headers={'Authorization': 'Bearer %s' % (self.token) },
        )

        return r.json()

    def publishPost(self,text,mediaIds=False):
        
        json = {'status': text}

        if mediaIds != False:
            json['media_ids'] = mediaIds

        r = requests.post(
            self.url + '/api/v1/statuses',
            json=json,
            headers={'Authorization': 'Bearer %s' % (self.token) },
        )
        return r.json()

    def makePost(self,author,date,link,text,mediaPath=[]):
        
        listText = self.convertTextPost(author,date,link,text)
       
        paginationIndex = 1
        paginationMax = len(listText)

        mediaIds = []
        for filePath in mediaPath:
            mediaIds.append(self.uploadMedia(filePath))

        idParentPost = self.publishPost(listText[0] + "\n" + "%d/%d" % (paginationIndex,paginationMax),mediaIds )
 
        for i in range(paginationIndex,paginationMax,1):
            self.replyPost(listText[i] + "\n" + "%d/%d" % (i+1,paginationMax),idParentPost["id"])
        
        return idParentPost["url"]
