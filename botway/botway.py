#!/usr/bin/python3
import requests,yaml,argparse,os,sys,lzma,fnmatch,json,datetime,time,shutil
from requests.exceptions import HTTPError
from pprint import pprint
from consolelog import *
from mastodon import *
from insta import *

def loadYamlFile(pathFile):
    return yaml.safe_load(open(pathFile))

def saveYamlFile(pathFile,data):
    with open(pathFile, 'w') as file:
        yaml.dump(data, file, default_flow_style=False, indent=True, encoding='utf-8', width=10000, allow_unicode=True)

def mergeSocialNetworkAccounts(socialNetworksConfig,socialNetworkAccountsHistory):
    socialNetworkMerged = socialNetworkAccountsHistory
    for socialNetwork in socialNetworksConfig:
        if socialNetwork not in socialNetworkMerged:
            socialNetworkMerged[socialNetwork] = {}
        for account in socialNetworksConfig[socialNetwork]:
            if account not in  socialNetworkMerged[socialNetwork]:
                socialNetworkMerged[socialNetwork][account] = []

    return socialNetworkMerged

def downloadPostsInstagramAccounts(config, instagramAccounts):
    instagram = Insta(config["user_instagram"],config["password_instagram"],config["user_agent"])
  
    for account in instagramAccounts:
        consoleLogOut.output(" Checking account: %s" % (account), "info")
        postShortCodes = instagram.getPostsShortCode(account,24) # 72 Hours, 3 days
        for postShortCode in postShortCodes:
            if postShortCode not in instagramAccounts[account]:
                instagram.downloadPost(account,postShortCode)
                consoleLogOut.output(" Instagram - Download post for account: %s  with url: %s" % (account, instagram.url + postShortCode) , "done")
                instagramAccounts[account].append(postShortCode)
            else:
                consoleLogOut.output(" Instagram - Previously downloaded post for account: %s  with url: %s" % (account, instagram.url + postShortCode) , "undone")

    return instagramAccounts

def findMediaFiles(pathDirectory):
    filesPath = []
    for file in os.listdir(pathDirectory):
        if ( fnmatch.fnmatch(file, '*.jpg') or 
           fnmatch.fnmatch(file, '*.jpeg') or 
           fnmatch.fnmatch(file, '*.png') or 
           fnmatch.fnmatch(file, '*.gif') or 
           fnmatch.fnmatch(file, '*.mp3') or 
           fnmatch.fnmatch(file, '*.mp4') or 
           fnmatch.fnmatch(file, '*.mov') or 
           fnmatch.fnmatch(file, '*.mkv') or 
           fnmatch.fnmatch(file, '*.avi')):
            filesPath.append(pathDirectory + "/" + file)
    return filesPath

def findFileByExtesion(pathDirectory,extension):
    for file in os.listdir(pathDirectory):
        if fnmatch.fnmatch(file, '*.'+ extension):
            return file

def main():
    parser = argparse.ArgumentParser(description='Botway, bot to send toot to mostodon from Instagram and Twitter accounts.')
    parser.add_argument(
            '-p',
            '--path-history', 
            dest="pathHistory", 
            nargs='?', 
            default="~/.botway/history.yml", 
            help='Path to folder to store all historic actions about bot. Example: -p "~/.botway/history.yml"'
            )
    parser.add_argument(
            '-c',
            '--config', 
            nargs='?', 
            default="config/config.yml", 
            help='Path to configuration file. Example: -c "config.yml"'
            )
    parser.add_argument(
            '-v',
            '--verbose', 
            nargs='?', 
            default=0, 
            help='Verbose level Example: -v1. Range: 0-2. '
            )

    args = parser.parse_args()
    global consoleLogOut 
    consoleLogOut = ConsoleLog(int(args.verbose))

    # Load config file
    configurationPath = args.config
    config = loadYamlFile(os.path.abspath(sys.path[0])+"/"+configurationPath)

    # Load history file
    historyPath = os.path.expanduser(args.pathHistory)
    historyDir = os.path.dirname(os.path.abspath(historyPath))

   
    socialNeworkAccountsConfig = {}
    if not bool(config["accounts_instagram"]):
        socialNeworkAccountsConfig["account_instagram"] = []
    else:
        socialNeworkAccountsConfig["account_instagram"] = dict.fromkeys(config["accounts_instagram"])
    if not bool(config["accounts_twitter"]):
        socialNeworkAccountsConfig["account_twitter"] = []
    else:
        socialNeworkAccountsConfig["account_twitter"] = dict.fromkeys(config["account_twitter"])

    if not os.path.exists(historyDir):
        # Create path if not exists
        os.makedirs(historyDir)
    if not os.path.exists(historyPath):
        # Create historic file if not exists
        saveYamlFile(historyPath,socialNeworkAccountsConfig)
        socialNetworkAccountsHistory = loadYamlFile(historyPath)
        consoleLogOut.output(" Project history file created: " + historyPath, "info")
    else:
        socialNetworkAccountsHistory = loadYamlFile(historyPath)

    socialNetwork = mergeSocialNetworkAccounts(socialNeworkAccountsConfig,socialNetworkAccountsHistory)

    consoleLogOut.output(" Load project history: " + historyPath, "info")

    socialNetwork["account_instagram"] = downloadPostsInstagramAccounts(config, socialNetwork["account_instagram"])
    
    masto = Mastodon(config["url_mastodon"],config["token_mastodon"],config["header_text"],config["footer_text"])

    for account in socialNetwork["account_instagram"]:
        for postCode in socialNetwork["account_instagram"][account]:
            pathPost = "%s-%s" % (account,postCode)
            if os.path.exists( pathPost ):
                consoleLogOut.output(" Post %s downloaded, preparing to post in mastodon..." % (pathPost), "info")

                textFile = findFileByExtesion(pathPost,"txt")
                text = open(pathPost + '/' + textFile).read()

                infoFile = findFileByExtesion(pathPost,"xz")
                jsonData = json.loads(lzma.open(pathPost + '/' + infoFile).read())
                datePost = datetime.fromtimestamp(jsonData["node"]["taken_at_timestamp"]).strftime('%Y-%m-%d %H:%M:%S')
                
                mediaFiles = findMediaFiles(pathPost)
                
                link =  "https://www.instagram.com/p/"+ jsonData["node"]["shortcode"]
                postUrl = masto.makePost(account,datePost,link,text,mediaFiles)
                consoleLogOut.output(" Post published on Mastodon! %s" % (postUrl), "done")
                consoleLogOut.output(" Removing post folder: %s" % (pathPost), "info")
                shutil.rmtree((pathPost))
                time.sleep(2)
        
    saveYamlFile(historyPath,socialNetwork)

if __name__ == "__main__":
    main()
